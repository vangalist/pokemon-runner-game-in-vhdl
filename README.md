# Pokemon Runner Game in VHDL for FPGA Board

This project is final project of Boğaziçi University EE242 Course. Aim of the project is to display abilities of FPGA board.
It has 3 Main parts and a lot of sub parts:

- VGA Driver to communicate with LCD screen via VGA Port.

- Input Manager to get input data of push buttons on the FPGA board. Ash is controlled via Push Buttons.

- Game Generator to generate game map, pokemons, Ash and control game events such as if an pokemon is catched.


Gameplay Video  [here] . 

[here]: <https://www.youtube.com/watch?v=MUDsuGY6xnY>